CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [En cours]

## [1.0.2] - 2019.10.01

### Update
- Mise à jour de la dépendance webapp-bundle

## [1.0.1] - 2018.01.15

### Update
- Mise à jour de la dépendance webapp-bundle

## [1.0.0] - 2018.07.12

### Ajout
- release initiale